import React from 'react';
import style from './Header.module.css';
import {NavLink} from "react-router-dom";

function HeaderTegLink() {

    const styleActive = {
        backgroundColor: "aliceblue",
        color: "black",
    }

    return (
        <div className={style.header}>
            <div className={style.title}>
                <NavLink exact to={'/'} activeStyle={styleActive}>Тема доклада</NavLink>
            </div>
            <div className={style.title}>
                <NavLink to={'/topic'} activeStyle={styleActive}>Основные компоненты</NavLink>
            </div>
            <div className={style.title}>
                <NavLink to={'/basic'} activeStyle={styleActive}>Какой Router выбрать</NavLink>
            </div>
            <div className={style.title}>
                <NavLink to={'/router'} activeStyle={styleActive}>Router</NavLink>
            </div>
            <div className={style.title}>
                <NavLink to={'/route'} activeStyle={styleActive}>Route</NavLink>
            </div>
            <div className={style.title}>
                <NavLink to={'/switch'} activeStyle={styleActive}>Switch</NavLink>
            </div>
            <div className={style.title}>
                <NavLink to={'/link'} activeStyle={styleActive}>Link</NavLink>
            </div>
            <div className={style.title}>
                <NavLink to={'/redirect'} activeStyle={styleActive}>Redirect</NavLink>
            </div>
        </div>
    );
}

export default HeaderTegLink;