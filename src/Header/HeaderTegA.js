import React from 'react';
import style from './Header.module.css';


function HeaderTegLink() {
    return (
        <div className={style.header}>
            <div className={style.title}>
                <a href={'/'}>Тема доклада</a>
            </div>
            <div className={style.title}>
                <a href={'/topic'}>Основные компоненты</a>
            </div>
            <div className={style.title}>
                <a href={'/basic'}>Какой Router выбрать</a>
            </div>
            <div className={style.title}>
                <a href={'/router'}>Router</a>
            </div>
            <div className={style.title}>
                <a href={'/route'}>Route</a>
            </div>
            <div className={style.title}>
                <a href={'/switch'}>Switch</a>
            </div>
            <div className={style.title}>
                <a href={'/link'}>Link</a>
            </div>
            <div className={style.title}>
                <a href={'/redirect'}>Redirect</a>
            </div>
        </div>
    );
}

export default HeaderTegLink;