import React from 'react';
import style from './Header.module.css';
import {Link} from "react-router-dom";

function HeaderTegLink() {
    return (
        <div className={style.header}>
            <div className={style.title}>
                <Link exact to={'/'}>Тема доклада</Link>
            </div>
            <div className={style.title}>
                <Link to={'/topic'}>Основные компоненты</Link>
            </div>
            <div className={style.title}>
                <Link to={'/basic'}>Какой Router выбрать</Link>
            </div>
            <div className={style.title}>
                <Link to={'/router'}>Router</Link>
            </div>
            <div className={style.title}>
                <Link to={'/route'}>Route</Link>
            </div>
            <div className={style.title}>
                <Link to={'/switch'}>Switch</Link>
            </div>
            <div className={style.title}>
                <Link to={'/link'}>Link</Link>
            </div>
            <div className={style.title}>
                <Link to={'/redirect'}>Redirect</Link>
            </div>
        </div>
    );
}

export default HeaderTegLink;