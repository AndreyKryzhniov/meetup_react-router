import React, {useState} from 'react';
import style from './App.module.css';
import HeaderTegLink from "./Header/HeaderTegLink";
import {Route, Switch} from "react-router-dom";
import PageComponent from "./PageComponent/PageComponent";
import initialData from './data.json'
import react_router_icon from "./img/react-router.png"
import primary_components from "./img/primary_component.png"
import basic_img from "./img/basic_img.png"
import route_img from "./img/route_img.png"
import switch_img from "./img/switch_img.png"
import link_img from "./img/link_img.png"
import redirect_img from "./img/redirect_img.png"
import error_img from "./img/error_img.png"
import error_second_img from "./img/error_second_img.png"
import HeaderTegA from "./Header/HeaderTegA";
import HeaderTegNavLink from "./Header/HeaderTegNavLink";

function App() {

    const data = initialData;

    const [value, setValue] = useState(false);

    const redirectToStartPage = () => {
        setValue(!value)
    }

    return (
        <div className={style.app}>
            <div className={style.header_wrapper}>
                <HeaderTegNavLink/>
            </div>
            <div className={style.page_wrapper}>
                <Switch>
                  {/*  <Route exact path={'/'} render={() => <PageComponent*/}
                  {/*      value = {value}*/}
                  {/*      redirectToStartPage={redirectToStartPage}*/}
                  {/*      title={"Доклад на тему React-Router"}*/}
                  {/*      description={data.name}*/}
                  {/*      images={react_router_icon}/>}/>*/}
                  {/*  <Route path={'/topic'} render={() => <PageComponent*/}
                  {/*      value = {value}*/}
                  {/*      redirectToStartPage={redirectToStartPage}*/}
                  {/*      title={"Основные компоненты"}*/}
                  {/*      description={data.topic}*/}
                  {/*      images={primary_components}/>}/>*/}
                  {/*  />}/>*/}
                  {/*  <Route path={'/route'} render={() => <PageComponent*/}
                  {/*      value = {value}*/}
                  {/*      redirectToStartPage={redirectToStartPage}*/}
                  {/*      title={"Route/:id"}*/}
                  {/*      description={data.route}*/}
                  {/*      images={route_img}*/}
                  {/*  />}/>*/}
                  <Route path={'/route'} render={() => <PageComponent
                    value = {value}
                    redirectToStartPage={redirectToStartPage}
                    title={"Route/:id"}
                    description={data.route}
                    images={route_img}
                  />}/>
                  {/*<Route path={'/route'} render={() => <PageComponent*/}
                  {/*  value = {value}*/}
                  {/*  redirectToStartPage={redirectToStartPage}*/}
                  {/*  title={"Route/:id"}*/}
                  {/*  description={data.route}*/}
                  {/*  images={route_img}*/}
                  {/*/>}/>*/}
                  {/*  <Route path={'/switch'} render={() => <PageComponent*/}
                  {/*      value = {value}*/}
                  {/*      redirectToStartPage={redirectToStartPage}*/}
                  {/*      title={"Switch"}*/}
                  {/*      description={data.switch}*/}
                  {/*      images={switch_img}*/}
                  {/*  />}/>*/}
                  {/*  <Route path={'/link'} render={() => <PageComponent*/}
                  {/*      value = {value}*/}
                  {/*      redirectToStartPage={redirectToStartPage}*/}
                  {/*      title={"Link"}*/}
                  {/*      description={data.link}*/}
                  {/*      images={link_img}*/}
                  {/*  />}/>*/}
                  {/*  <Route path={'/redirect'} render={() => <PageComponent*/}
                  {/*      value = {value}*/}
                  {/*      redirectToStartPage={redirectToStartPage}*/}
                  {/*      title={"Redirect"}*/}
                  {/*      description={data.redirect}*/}
                  {/*      images={redirect_img}*/}
                  {/*  />}/>*/}
                  {/*  <Route path={'/'} render={() => <PageComponent*/}
                  {/*      value = {value}*/}
                  {/*      redirectToStartPage={redirectToStartPage}*/}
                  {/*      title={"Error"}*/}
                  {/*      description={error_second_img}*/}
                  {/*      images={error_img}*/}
                  {/*  />}/>*/}
                </Switch>
            </div>
        </div>
    );
}

export default App;
