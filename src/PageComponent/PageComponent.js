import React from 'react';
import style from './PageComponent.module.css';
import {Redirect} from "react-router-dom";

function PageComponent(props) {

    if (props.value) {
        props.redirectToStartPage()
        return <Redirect to={'/redirect'}/>}

    const redirectToStartPage = () => {
        props.redirectToStartPage()
    }


    const styleTitle = props.title === 'Error' ? style.error : style.title

    return (
        <div className={style.page_component}>
            <div className={styleTitle}>
                <h1>{props.title}</h1>
            </div>
            <div className={style.info}>
                <div className={style.information}>
                    {props.title === 'Error'
                        ? <img src = {props.description}/>
                        : <h3 className={style.text}>{props.description}</h3>}
                </div>
                <div className={style.img}>
                    <img src={props.images}/>
                </div>
            </div>
            <button onClick={redirectToStartPage}>Redirect</button>
        </div>
    );
}

export default PageComponent;